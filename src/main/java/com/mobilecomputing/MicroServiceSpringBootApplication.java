package com.mobilecomputing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Fuente: https://www.adictosaltrabajo.com/tutoriales/desarrollo-de-microservicios-con-spring-boot-y-docker/
 */

@SpringBootApplication
public class MicroServiceSpringBootApplication {

	public static void main(String[] args) {

		SpringApplication.run(MicroServiceSpringBootApplication.class, args);
	}
}
