package com.mobilecomputing;

public interface AddressService {

	String getServerAddress() throws Exception;
}
