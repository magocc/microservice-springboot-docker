package com.mobilecomputing;

import org.springframework.stereotype.Service;

import java.net.InetAddress;

@Service
public class AddressServiceImpl implements AddressService {

	public String getServerAddress() throws Exception {

		return InetAddress.getLocalHost().getHostAddress();

	}

}
