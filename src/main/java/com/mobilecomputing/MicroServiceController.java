package com.mobilecomputing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MicroServiceController {

	private final AddressService service;

	@Autowired
	public MicroServiceController(AddressService service) {
		this.service = service;
	}

	@RequestMapping(value = "/servicio")
	public String hello() throws Exception {

		String serverAddress = service.getServerAddress();
		return "MAEAMEEE!! La ip que devuelve el servicio es: " + serverAddress + "\n";
	}


}
